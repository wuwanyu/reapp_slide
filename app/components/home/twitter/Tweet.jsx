import React from 'react';
import { List} from 'reapp-kit';
import TweetTitle from './TweetTitle';

export default React.createClass({
  render() {
    var { name, index, handle, children, ...props } = this.props;

    return (
      <List.Item
        index={index}
        before={<img src='' />}
        title={<TweetTitle name={name} handle={handle} />}
        {...this.props}>
        {children}
      </List.Item>
    );
  }
});