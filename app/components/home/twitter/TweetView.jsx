import { React,View,Button,Icon,List,Bar} from 'reapp-kit';
import { RoutedViewListMixin} from 'reapp-routes';
import { Link,RouteHandler} from 'react-router';



export default React.createClass({
  render() {
    return (
      <View {...this.props} title="View Tweet">
        <List>
          <Tweet
            name="dlongster"
            handle="@dlongster">
            Lorem ipsum dolor sit amet, man!
          </Tweet>
        </List>
      </View>
    );
  }
});