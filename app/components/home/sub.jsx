import {React, DottedViewList,NestedViewList,Button, View, List ,store } from 'reapp-kit';
/*


export default class extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            step:0
        };
    }

    render() {
        return(
        <View {...this.props} >
            <NestedViewList
                scrollToStep:{this.state.step} >

                <View title="one">
                    <List wrap>
                        <p onTap={() => {this.setState({ step: this.state.step++ })}}>first view contents</p>
                    </List>
                </View>

                <View title="two">
                    <p>Second view contents</p>
                </View>

                <View title="three">
                    <p>three view contents</p>
                </View>




            </NestedViewList>
        </View>
        );
    }

}




*/


export default class extends React.Component {

    constructor(props){
        super(props);
        this.state={
            step: 0
        };
    }

      forwardStep(){
          if(this.state.step+1>2){
              return;
          }else{
              console.log("forwardStep");
              this.setState({step:this.state.step++});
              console.log(this.state.step);
              store().set('step',this.state.step);
          }
      }
        backStep(){
            if(this.state.step-1<0){
                return;
            }else{
                console.log("backStep");
                this.setState({step:this.state.step--});
                console.log(this.state.step);
                store().set('step',this.state.step);
            }
        }

    render() {
        const store = this.context.store();
        this.state.step = store.get("step");

        return (
            <View {...this.props} >
                <NestedViewList
                    scrollToStep={this.state.step}>

                    <View >
                        <List wrap>
                            <Button onTap={ this.backStep.bind(this)}>back</Button>
                            <Button onTap={ this.forwardStep.bind(this)}>0 view contents</Button>
                        </List>
                    </View>

                    <View >
                        <List wrap>
                            <Button onTap={ this.backStep.bind(this)}>back</Button>
                            <Button onTap={this.forwardStep.bind(this)}>1 view contents</Button>
                        </List>
                    </View>

                    <View >
                        <List wrap>
                            <Button onTap={ this.backStep.bind(this)}>back</Button>
                            <Button onTap={this.forwardStep.bind(this)}>2 view contents</Button>
                        </List>
                    </View>

                </NestedViewList>
            </View>
        );
    }
}