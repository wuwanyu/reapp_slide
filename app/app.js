import './theme';
import './store';

import { router, route } from 'reapp-kit';

router(require,
  route('home', '/',
      route('sub'),
      route('twitter',
          route('tweetView')
      )
  )
);